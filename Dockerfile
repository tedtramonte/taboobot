FROM rust:1.70.0@sha256:508253a23bfe549d8afb3027198e1882c2b09fd34898a83163a23f395f410b11 as builder
ARG CARGO_MAKE_PROFILE="production"
WORKDIR /taboobot
RUN cargo install --force cargo-make
COPY ./Cargo.toml Cargo.toml
COPY ./Cargo.lock Cargo.lock
# Building with a dummy main.rs allows Docker
# to cache compiled build dependencies
RUN mkdir ./src && echo 'fn main() { println!("Dummy!"); }' > ./src/main.rs
COPY ./Makefile.toml Makefile.toml
RUN cargo make --profile ${CARGO_MAKE_PROFILE} build
RUN rm -rf ./src
# Now bring in the actual source code to build
COPY ./src ./src
RUN touch -a -m ./src/main.rs
RUN cargo make --profile ${CARGO_MAKE_PROFILE} build


FROM debian:buster-slim@sha256:738002a6e94e8629a0f869be8ea56519887979d5ed411486441981a8b16f2fc8

# Build arguments
ARG S6_OVERLAY_VERSION="v2.2.0.3"

# Install s6 Overlay process manager (https://github.com/just-containers/s6-overlay)
ADD https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64-installer /tmp/
RUN chmod +x /tmp/s6-overlay-amd64-installer && /tmp/s6-overlay-amd64-installer / && rm /tmp/s6-overlay-amd64-installer

RUN apt-get update && apt-get install -y \
  ca-certificates \
  libssl-dev \
  && rm -rf /var/lib/apt/lists/*

# Install taboobot
COPY --from=builder /taboobot/dist/taboobot /usr/local/bin/taboobot

# TabooBot Defaults
ENV LOG_LEVEL=debug
ENV LOG_STYLE=always
ENV SENTRY_DSN=
ENV REDIS_USER=
ENV REDIS_PASSWORD=
ENV REDIS_HOST=127.0.0.1
ENV REDIS_PORT=6379
ENV REDIS_DB=
ENV BOT_TOKEN=
ENV APPLICATION_ID=
ENV USE_ERROR_REPORTING=false
ENV USE_TRACING=false

# S6 Configuration
ENV S6_READ_ONLY_ROOT=1

# Drop root
RUN addgroup taboobot && adduser --ingroup taboobot taboobot
USER taboobot

ENTRYPOINT ["/init"]
CMD ["taboobot"]
