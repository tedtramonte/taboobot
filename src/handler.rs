use std::borrow::Cow;
use std::collections::BTreeMap;
use std::convert::TryInto;
use std::io::Cursor;
use std::time::Duration;

use image::{DynamicImage, Rgb, RgbImage};
use imageproc::{
    drawing::{draw_filled_rect_mut, draw_text_mut},
    rect::Rect,
};
use regex::RegexSetBuilder;
use rusttype::{Font, Scale};
use serenity::{
    async_trait,
    model::{
        channel::{AttachmentType, Message},
        event::MessageUpdateEvent,
        gateway::Ready,
        interactions::{
            application_command::{
                ApplicationCommand, ApplicationCommandInteractionDataOptionValue,
                ApplicationCommandOptionType,
            },
            Interaction, InteractionApplicationCommandCallbackDataFlags, InteractionResponseType,
        },
        timestamp::Timestamp,
    },
    prelude::*,
};
use strip_markdown::*;
use tracing::{debug, debug_span, error, info, trace};

use crate::guild::Guild;
use crate::redis_connection::RedisConnectionManager;
use crate::taboo::Taboo;

const FONT_DATA: &[u8] = include_bytes!("./fonts/OpenSans-Regular.ttf");

#[tracing::instrument(level = "trace")]
async fn find_taboos(guild: Guild, message: &str) -> Vec<Taboo> {
    debug!(?guild.taboos);

    let mut patterns: Vec<(&Taboo, String)> = Vec::new();
    for taboo in &guild.taboos {
        let _span = debug_span!("build_pattern", ?taboo).entered();
        let pattern = format!("{}{}{}", r"\b", &taboo.phrase, r"\b");
        patterns.push((taboo, pattern));
        if let Some(alt_phrases) = &taboo.alternative_phrases {
            for alt_phrase in alt_phrases {
                let pattern = format!("{}{}{}", r"\b", alt_phrase, r"\b");
                patterns.push((taboo, pattern));
            }
        }
    }

    debug!(?patterns);

    let bad_characters = [
        '\u{200B}', '\u{200C}', '\u{200D}', '\u{200E}', '\u{200F}', '\u{FEFF}',
    ];

    let matches: Vec<usize> = RegexSetBuilder::new(patterns.iter().map(|p| &p.1))
        .case_insensitive(true)
        .build()
        .unwrap()
        .matches(&strip_markdown(message).replace(&bad_characters, ""))
        .iter()
        .collect();

    debug!(?matches);

    let found: Vec<Taboo> = matches.iter().map(|&m| patterns[m].0.clone()).collect();

    debug!(?found);
    found
}

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn guild_create(&self, ctx: Context, guild: serenity::model::guild::Guild, is_new: bool) {
        if is_new {
            info!("Joined is_new");
            if let Some(channel) = guild.default_channel(ctx.cache.current_user_id()).await {
                let message_content = "Thanks for trying TabooBot. Before anyone gets started, a server administrator should configure the roles and permissions for each command. This can be done by navigating to Server Settings > Integrations > TabooBot.";
                if let Err(why) = channel.say(&ctx.http, message_content).await {
                    sentry::capture_error(&why);
                    error!("Couldn't send welcome message: {}", why);
                }
            }
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn message(&self, ctx: Context, message: Message) {
        // Since the bot specifies which taboo was broken in its message
        // this prevents recursion by ignoring messages from the bot itself
        if ctx.cache.current_user_id() == message.author.id {
            trace!("Message is from the bot itself");
            return;
        }

        // Sentry data
        sentry::configure_scope(|scope| {
            scope.set_user(Some(sentry::User {
                id: Some(message.author.id.to_string()),
                username: Some(format!(
                    "{}#{}",
                    &message.author.name, message.author.discriminator
                )),
                ..Default::default()
            }));

            let mut context = BTreeMap::new();
            context.insert(
                String::from("guild"),
                message.guild_id.unwrap_or_default().to_string().into(),
            );
            context.insert(String::from("content"), message.content.clone().into());
            scope.set_context("message", sentry::protocol::Context::Other(context));
        });

        if let Some(guild_id) = message.guild_id {
            let mut ctx_data = ctx.data.write().await;
            let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

            let guild = Guild::new(redis, guild_id).await;

            let found = find_taboos(guild, &message.content).await;

            for taboo in found {
                debug!(?taboo);
                let mut taboo = taboo.to_owned();
                taboo.increment_count(redis).await;
                match taboo.last_seen {
                    Some(last_seen) => {
                        let difference = Duration::from_secs(
                            (message.timestamp.unix_timestamp() - last_seen.unix_timestamp())
                                as u64,
                        );
                        let mut f_difference = timeago::Formatter::new();
                        f_difference.num_items(3);
                        f_difference.too_low("just now");
                        let mut f_streak = timeago::Formatter::new();
                        f_streak.num_items(3);
                        f_streak.ago("");

                        let response_content: String;

                        match taboo.longest_streak {
                            Some(longest_streak) => {
                                if difference > longest_streak {
                                    // This is the new longest_streak
                                    response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nThis is your new record! Your previous best was {}.",
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference),
                                        f_streak.convert(longest_streak)
                                    );
                                    taboo
                                        .update_longest_streak(redis, difference.as_secs())
                                        .await;
                                } else {
                                    // This is not the new longest_streak
                                    response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nYour longest streak is {}.",
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference),
                                        f_streak.convert(longest_streak)
                                    );
                                }
                            }
                            None => {
                                response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.",
                                    taboo.phrase,
                                    taboo.count,
                                    f_difference.convert(difference)
                                );
                                taboo
                                    .update_longest_streak(redis, difference.as_secs())
                                    .await;
                            }
                        }

                        if let Err(why) = message.reply(&ctx.http, response_content).await {
                            sentry::capture_error(&why);
                            error!("Couldn't reply: {}", why);
                        }
                    }
                    None => {
                        // This is the first time taboo has been seen
                        let response_content = format!("You have broken the taboo placed on \"{}\". This is the first time this taboo has been broken.",
                            taboo.phrase
                        );
                        if let Err(why) = message.reply(&ctx.http, response_content).await {
                            sentry::capture_error(&why);
                            error!("Couldn't reply: {}", why);
                        }
                    }
                }
                taboo.update_last_seen(redis, message.timestamp).await;
            }
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn message_update(
        &self,
        ctx: Context,
        _old_message: Option<Message>,
        _message: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        // Rich embedded content seems to be created as a message_update event
        // with no author. The event doesn't seem to indicate that it was
        // to create an embed, so this simply skips the event if there's no author
        if event.clone().author.is_none() {
            trace!("Message update is (probably) from Discord itself.");
            return;
        }

        let author = event
            .clone()
            .author
            .expect("Error getting an author in message_update.");
        let content = event
            .clone()
            .content
            .expect("Error getting content in message_update.");
        let timestamp = match event.edited_timestamp {
            Some(ts) => ts,
            None => {
                trace!("Message update has no timestamp. This is (probably) a pin event.");
                return;
            }
        };

        // Since the bot specifies which taboo was broken in its message
        // this prevents recursion by ignoring message_updates from the bot itself
        if ctx.cache.current_user_id() == author.id {
            trace!("Message update is from the bot itself");
            return;
        }

        // Sentry data
        sentry::configure_scope(|scope| {
            scope.set_user(Some(sentry::User {
                id: Some(author.id.to_string()),
                username: Some(author.tag()),
                ..Default::default()
            }));

            let mut context = BTreeMap::new();
            context.insert(
                String::from("guild"),
                event.guild_id.unwrap_or_default().to_string().into(),
            );
            context.insert(String::from("content"), content.clone().into());
            scope.set_context("message_update", sentry::protocol::Context::Other(context));
        });

        if let Some(guild_id) = event.guild_id {
            let mut ctx_data = ctx.data.write().await;
            let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

            let guild = Guild::new(redis, guild_id).await;

            let found = find_taboos(guild, &content).await;

            for taboo in found {
                debug!(?taboo);
                let mut taboo = taboo.to_owned();
                taboo.increment_count(redis).await;
                match taboo.last_seen {
                    Some(last_seen) => {
                        let difference = Duration::from_secs(
                            (timestamp.unix_timestamp() - last_seen.unix_timestamp()) as u64,
                        );
                        let mut f_difference = timeago::Formatter::new();
                        f_difference.num_items(3);
                        f_difference.too_low("just now");
                        let mut f_streak = timeago::Formatter::new();
                        f_streak.num_items(3);
                        f_streak.ago("");

                        let response_content: String;

                        match taboo.longest_streak {
                            Some(longest_streak) => {
                                if difference > longest_streak {
                                    // This is the new longest_streak
                                    response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nThis is your new record! Your previous best was {}.\n\nEvidence: {}",
                                        author.id,
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference),
                                        f_streak.convert(longest_streak),
                                        event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                    );
                                    taboo
                                        .update_longest_streak(redis, difference.as_secs())
                                        .await;
                                } else {
                                    // This is not the new longest_streak
                                    response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nYour longest streak is {}.\n\nEvidence: {}",
                                        author.id,
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference),
                                        f_streak.convert(longest_streak),
                                        event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                    );
                                }
                            }
                            None => {
                                response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\n\nEvidence: {}",
                                    author.id,
                                    taboo.phrase,
                                    taboo.count,
                                    f_difference.convert(difference),
                                    event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                );
                                taboo
                                    .update_longest_streak(redis, difference.as_secs())
                                    .await;
                            }
                        }

                        if let Err(why) = event
                            .channel_id
                            .send_message(&ctx.http, |message| message.content(response_content))
                            .await
                        {
                            sentry::capture_error(&why);
                            error!("Couldn't respond to message_update: {}", why);
                        }
                    }
                    None => {
                        // This is the first time taboo has been seen
                        let response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This is the first time this taboo has been broken.\nEvidence: {}",
                            author.id,
                            taboo.phrase,
                            event.id.link_ensured(&ctx.http, event.channel_id, None).await
                        );
                        if let Err(why) = event
                            .channel_id
                            .send_message(&ctx.http, |message| message.content(response_content))
                            .await
                        {
                            sentry::capture_error(&why);
                            error!("Couldn't respond to message_update: {}", why);
                        }
                    }
                }
                taboo.update_last_seen(redis, timestamp).await;
            }
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        match interaction {
            Interaction::ApplicationCommand(interaction) => {
                // Sentry data
                sentry::configure_scope(|scope| {
                    scope.set_user(Some(sentry::User {
                        id: Some(interaction.user.id.to_string()),
                        username: Some(format!(
                            "{}#{}",
                            &interaction.user.name, interaction.user.discriminator
                        )),
                        ..Default::default()
                    }));

                    let mut context = BTreeMap::new();
                    context.insert(
                        String::from("guild"),
                        interaction.guild_id.unwrap_or_default().to_string().into(),
                    );
                    context.insert(String::from("kind"), interaction.kind.num().into());
                    context.insert(String::from("name"), interaction.data.name.clone().into());
                    scope.set_context(
                        "ApplicationCommand",
                        sentry::protocol::Context::Other(context),
                    );
                });

                // Throw away any interactions not sent within a guild
                if let Some(guild_id) = interaction.guild_id {
                    let mut ctx_data = ctx.data.write().await;
                    let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

                    let mut guild = Guild::new(redis, guild_id).await;

                    match interaction.data.name.as_str() {
                        "add" => {
                            if let ApplicationCommandInteractionDataOptionValue::String(
                                taboo,
                            ) =
                                interaction
                                    .data
                                    .options
                                    .get(0)
                                    .unwrap()
                                    .resolved
                                    .as_ref()
                                    .expect("Expected string option")
                            {
                                let mut alt_phrases = Vec::new();
                                for alt_phrase in &interaction.data.options[1..] {
                                    if let ApplicationCommandInteractionDataOptionValue::String(phrase) = alt_phrase.resolved.as_ref().expect("Expected string option") {
                                        alt_phrases.push(phrase.to_string());
                                    }
                                }

                                if !alt_phrases.is_empty() {
                                    guild.set_taboo(redis, taboo, Some(alt_phrases)).await;
                                } else {
                                    guild.set_taboo(redis, taboo, None).await;
                                }

                                if let Err(why) = interaction
                                    .create_interaction_response(&ctx.http, |response| {
                                        response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                            data.content(format!("You have placed a taboo on \"{}\".", taboo))
                                        })
                                    })
                                    .await
                                {
                                    sentry::capture_error(&why);
                                    error!("Couldn't respond to taboo creation: {}", why);
                                }
                            }
                        }

                        "delete" => {
                            if !guild.taboos.is_empty() {
                                if let Err(why) = interaction
                                    .create_interaction_response(&ctx.http, |response| {
                                        response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                            data
                                            .content("Which taboos do you wish to remove?")
                                            .components(|components| {
                                                components.create_action_row(|row| {
                                                    row.create_select_menu(|menu| {
                                                        menu.custom_id("delete_taboos")
                                                            .placeholder("Current taboos")
                                                            .min_values(1)
                                                            .max_values(guild.taboos.len().try_into().unwrap_or(u64::MAX))
                                                            .options(|mut options| {
                                                                for taboo in guild.taboos {
                                                                    options = options
                                                                        .create_option(
                                                                            |option| {
                                                                                option
                                                                            .label(
                                                                                &taboo.phrase,
                                                                            )
                                                                            .value(
                                                                                &taboo.phrase,
                                                                            )
                                                                            },
                                                                        )
                                                                }
                                                                options
                                                            })
                                                    })
                                                })
                                            })
                                            .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                        })
                                    })
                                    .await
                                {
                                    sentry::capture_error(&why);
                                    error!("Couldn't respond to taboo deletion: {}", why);
                                }
                            } else if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                    response
                                        .kind(InteractionResponseType::ChannelMessageWithSource)
                                        .interaction_response_data(|data| {
                                            data.content(
                                                "There are currently no taboos in place.",
                                            )
                                            .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                        })
                                })
                                .await
                            {
                                sentry::capture_error(&why);
                                error!("Couldn't respond to taboo deletion: {}", why);
                            }
                        }

                        "list" => {
                            guild.taboos.sort_by(|a, b| a.phrase.cmp(&b.phrase));

                            for option in &interaction.data.options {
                                match option.name.as_ref() {
                                    "sort" => if let Some(ApplicationCommandInteractionDataOptionValue::String(sort)) = option.resolved.as_ref() {
                                        match sort.as_ref() {
                                            "count" => guild.taboos.sort_by(|a, b| b.count.cmp(&a.count)),
                                            "phrase" => guild.taboos.sort_by(|a, b| a.phrase.cmp(&b.phrase)),
                                            "longest_streak" => guild.taboos.sort_by(|a, b| b.longest_streak.cmp(&a.longest_streak)),
                                            "last_seen" => guild.taboos.sort_by(|a, b| b.last_seen.cmp(&a.last_seen)),
                                            _ => {}
                                        }
                                    }
                                    "reverse" => if let Some(ApplicationCommandInteractionDataOptionValue::Boolean(reverse)) = option.resolved.as_ref() {
                                        if *reverse {
                                            guild.taboos.reverse();
                                        }
                                    }
                                    _ => {}
                                }
                            }

                            match guild.taboos.len() {
                                len if len > 0 => {
                                    let current_time = Timestamp::now();

                                    let mut rows: Vec<String> = Vec::new();
                                    let mut taboos = guild.taboos.iter().peekable();

                                    while let Some(taboo) = taboos.next()  {
                                        let mut since = "never".to_owned();
                                        if let Some(last_seen) = taboo.last_seen {
                                            let difference = Duration::from_secs(
                                                (current_time.unix_timestamp() - last_seen.unix_timestamp())
                                                    as u64,
                                            );
                                            let mut f_since = timeago::Formatter::new();
                                            f_since.num_items(2);
                                            f_since.too_low("just now");
                                            since = f_since.convert(difference);
                                        }

                                        let mut streak = "never".to_owned();
                                        if let Some(longest_streak) = taboo.longest_streak {
                                            let mut f_streak = timeago::Formatter::new();
                                            f_streak.num_items(2);
                                            f_streak.ago("");
                                            streak = f_streak.convert(longest_streak)
                                        }

                                        if let Some(phrases) = &taboo.alternative_phrases {
                                            rows.push(format!("{}x - {}: {}", taboo.count, taboo.phrase, phrases.join(", ")));
                                        } else {
                                            rows.push(format!("{}x - {}", taboo.count, taboo.phrase));
                                        }
                                        rows.push(format!("Longest Streak: {}", streak));
                                        rows.push(format!("Last seen {}", since));

                                        if taboos.peek().is_some() {
                                            rows.push("".to_string());
                                        }
                                    }

                                    let font = Font::try_from_bytes(FONT_DATA).unwrap();
                                    let line_height: u32 = 30;
                                    let margin: u32 = 15;
                                    let watermark_scale = Scale {
                                        x: 15.0,
                                        y: 15.0,
                                    };
                                    let scale = Scale {
                                        x: line_height as f32,
                                        y: line_height as f32,
                                    };

                                    // rows currently has a heading added always, so this will never be None
                                    let longest_row = rows.iter().max_by_key(|r| r.len()).unwrap();
                                    // Since longest_row currently will never be None, unwrapping last() should be fine
                                    let image_width: u32 = if let Some(point) = font.layout(longest_row, scale, rusttype::point(0.0, 0.0)).last().unwrap().pixel_bounding_box() {
                                        point.max.x as u32 + (margin * 2)
                                    } else {
                                        error!("Couldn't calculate image_width for {:#?}", longest_row);
                                        400
                                    };
                                    let image_height: u32 = (rows.len() as u32 * line_height) + (margin * 2);
                                    let mut image = RgbImage::new(image_width, image_height);
                                    let background = Rect::at(0,0).of_size(image_width, image_height);
                                    let background_color =  Rgb([50, 53, 59]);

                                    draw_filled_rect_mut(&mut image, background, background_color);
                                    draw_text_mut(&mut image, image::Rgb([255u8, 255u8, 255u8]), 2, 2, watermark_scale, &font, "Generated by TabooBot");
                                    for (i, row) in rows.iter().enumerate() {
                                        draw_text_mut(&mut image, image::Rgb([255u8, 255u8, 255u8]), margin.try_into().unwrap_or_default(), (margin + i as u32 * line_height).try_into().unwrap_or_default(), scale, &font, row);
                                    }

                                    let mut cursor = Cursor::new(Vec::new());
                                    DynamicImage::ImageRgb8(image).write_to(&mut cursor, image::ImageOutputFormat::Jpeg(255u8)).unwrap();

                                    if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                            response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                                data.add_file(AttachmentType::Bytes {
                                                        data: Cow::from(cursor.into_inner()),
                                                        filename: "taboos.jpeg".to_owned(),
                                                    }
                                                )
                                            })
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to list command {}", why);
                                    }
                                }
                                _ => {
                                    if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                            response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                                data
                                                    .content("There are currently no taboos in place.")
                                                    .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                            })
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to list command {}", why);
                                    }
                                }
                            }
                        }

                        _ => {}
                    }
                } else if let Err(why) = interaction
                    .create_interaction_response(&ctx.http, |response| {
                        response
                            .kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|data| {
                                data.content(
                                    "Nothing will ever be taboo between us. Take me to a guild.",
                                )
                            })
                    })
                    .await
                {
                    sentry::capture_error(&why);
                    error!("Couldn't respond to slash command: {}", why);
                }
            }

            Interaction::MessageComponent(interaction) => {
                // Sentry data
                sentry::configure_scope(|scope| {
                    scope.set_user(Some(sentry::User {
                        id: Some(interaction.user.id.to_string()),
                        username: Some(format!(
                            "{}#{}",
                            &interaction.user.name, interaction.user.discriminator
                        )),
                        ..Default::default()
                    }));

                    let mut context = BTreeMap::new();
                    context.insert(
                        String::from("guild"),
                        interaction.guild_id.unwrap_or_default().to_string().into(),
                    );
                    context.insert(String::from("kind"), interaction.kind.num().into());
                    scope.set_context(
                        "MessageComponent",
                        sentry::protocol::Context::Other(context),
                    );
                });

                // Throw away any interactions not sent within a guild
                if let Some(guild_id) = interaction.guild_id {
                    let mut ctx_data = ctx.data.write().await;
                    let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

                    let mut guild = Guild::new(redis, guild_id).await;

                    #[allow(clippy::single_match)]
                    match interaction.data.custom_id.as_str() {
                        "delete_taboos" => {
                            for (i, phrase) in interaction.data.values.to_owned().iter().enumerate()
                            {
                                // Rather than checking if the Taboo is still registered
                                // should the Guild struct keep track of the active /delete interaction id
                                // and pass that along in the interaction.data.custom_id?
                                if guild
                                    .taboos
                                    .iter()
                                    .any(|taboo| taboo.phrase == phrase.as_str())
                                {
                                    guild.delete_taboo(redis, phrase).await;
                                    if i == 0 {
                                        if let Err(why) = interaction
                                            .create_interaction_response(&ctx.http, |response| {
                                                response
                                                    .kind(InteractionResponseType::ChannelMessageWithSource)
                                                    .interaction_response_data(|data| {
                                                        data.content(format!(
                                                            "The taboo on \"{}\" has been removed.",
                                                            phrase
                                                        ))
                                                    })
                                            })
                                            .await
                                        {
                                            sentry::capture_error(&why);
                                            error!("Couldn't respond to taboo delete: {}", why);
                                        }
                                    } else if let Err(why) = interaction
                                        .create_followup_message(&ctx.http, |message| {
                                            message.content(format!(
                                                "The taboo on \"{}\" has been removed.",
                                                phrase
                                            ))
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to taboo delete: {}", why);
                                    }
                                } else {
                                    #[allow(clippy::collapsible_else_if)]
                                    if i == 0 {
                                        if let Err(why) = interaction
                                            .create_interaction_response(&ctx.http, |response| {
                                                response
                                                .kind(InteractionResponseType::ChannelMessageWithSource)
                                                .interaction_response_data(|data| {
                                                    data.content(format!(
                                                        "There is currently no taboo on \"{}\".",
                                                        phrase
                                                    ))
                                                })
                                            })
                                            .await
                                        {
                                            sentry::capture_error(&why);
                                            error!("Couldn't respond to taboo delete: {}", why);
                                        }
                                    } else if let Err(why) = interaction
                                        .create_followup_message(&ctx.http, |message| {
                                            message.content(format!(
                                                "There is currently no taboo on \"{}\".",
                                                phrase
                                            ))
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to taboo delete: {}", why);
                                    }
                                }
                            }
                            // Here would be a great spot to make the Ephemeral response unusable.
                            // However, Discord's API does not support doing basically anything
                            // with an Ephemeral response. Hopefully this changes.
                        }

                        _ => {}
                    }
                }
            }

            _ => {}
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let commands = ApplicationCommand::set_global_application_commands(&ctx.http, |commands| {
            commands
                .create_application_command(|command| {
                    command
                        .name("add")
                        .description("Create a new taboo in this server")
                        .create_option(|option| {
                            option
                                .name("phrase")
                                .description("The word/phrase to taboo")
                                .kind(ApplicationCommandOptionType::String)
                                .required(true)
                        })
                        .create_option(|option| {
                            option
                                .name("alt1")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt2")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt3")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt4")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt5")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt6")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt7")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt8")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt9")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                })
                .create_application_command(|command| {
                    command
                        .name("delete")
                        .description("Remove a taboo in this server")
                })
                .create_application_command(|command| {
                    command
                        .name("list")
                        .description("List all taboos in this server")
                        .create_option(|option| {
                            option
                                .name("sort")
                                .description("Sort by")
                                .kind(ApplicationCommandOptionType::String)
                                .add_string_choice("count", "count")
                                .add_string_choice("phrase", "phrase")
                                .add_string_choice("longest streak", "longest_streak")
                                .add_string_choice("last seen", "last_seen")
                        })
                        .create_option(|option| {
                            option
                                .name("reverse")
                                .description("Reverse sorting")
                                .kind(ApplicationCommandOptionType::Boolean)
                        })
                })
        })
        .await;

        info!("Registered commands: {:#?}", commands.unwrap())
    }
}
