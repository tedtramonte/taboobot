use redis::AsyncCommands;

use crate::taboo::Taboo;

#[derive(Debug, Clone)]
pub struct Guild {
    pub guild_id: serenity::model::id::GuildId,
    pub taboos: Vec<Taboo>,
}

impl Guild {
    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn new(
        redis: &mut redis::aio::ConnectionManager,
        guild_id: serenity::model::id::GuildId,
    ) -> Self {
        let mut g = Guild {
            guild_id,
            taboos: Vec::new(),
        };
        g.update_taboos(redis).await;
        g
    }

    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn delete_taboo(&mut self, redis: &mut redis::aio::ConnectionManager, phrase: &str) {
        let _: () = redis
            .del(self.guild_id.to_string() + ":" + phrase)
            .await
            .expect("Error deleting taboo");
        self.update_taboos(redis).await;
    }

    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn set_taboo(
        &mut self,
        redis: &mut redis::aio::ConnectionManager,
        phrase: &str,
        alternative_phrases: Option<Vec<String>>,
    ) {
        let _: () = redis
            .hset_nx(
                self.guild_id.to_string() + ":" + phrase,
                "guild_id",
                self.guild_id.to_string(),
            )
            .await
            .expect("Error setting taboo guild_id");
        let _: () = redis
            .hset_nx(self.guild_id.to_string() + ":" + phrase, "phrase", phrase)
            .await
            .expect("Error setting taboo phrase");
        let _: () = redis
            .hset_nx(self.guild_id.to_string() + ":" + phrase, "count", "0")
            .await
            .expect("Error setting taboo count");
        if let Some(phrases) = alternative_phrases {
            let _: () = redis
                .hset_nx(
                    self.guild_id.to_string() + ":" + phrase,
                    "alternative_phrases",
                    serde_json::to_string(&phrases)
                        .expect("Error serializing taboo alternative_phrases"),
                )
                .await
                .expect("Error setting taboo alternative_phrases");
        }

        self.update_taboos(redis).await;
    }

    #[allow(clippy::needless_collect)]
    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn update_taboos(&mut self, redis: &mut redis::aio::ConnectionManager) {
        let mut con = redis.clone();
        let mut r: redis::AsyncIter<'_, String> = con
            .scan_match(self.guild_id.to_string() + ":*")
            .await
            .expect("Error getting taboos from store");

        let mut new_taboos: Vec<Taboo> = Vec::new();
        while let Some(taboo) = r.next_item().await {
            let t: Taboo = redis
                .hgetall(taboo)
                .await
                .expect("Error getting taboo from store");
            new_taboos.push(t);
        }
        self.taboos = new_taboos;
    }
}
