# TabooBot
[![License](https://img.shields.io/badge/license-MIT-brightgreen)](https://choosealicense.com/licenses/mit/)
[![GitLab pipeline](https://img.shields.io/gitlab/pipeline-status/tedtramonte/taboobot)](https://gitlab.com/tedtramonte/taboobot/-/pipelines)
[![GitLab tag](https://img.shields.io/gitlab/v/tag/tedtramonte/taboobot?sort=semver)](https://gitlab.com/tedtramonte/taboobot/-/tags)
[![GitLab release](https://img.shields.io/gitlab/v/release/tedtramonte/taboobot?sort=semver)](https://gitlab.com/tedtramonte/taboobot/-/releases)
[![Production status](https://argocd.tramonte.us/api/badge?name=taboobot)]( https://argocd.tramonte.us/applications/taboobot)

TabooBot is an open source bot for your Discord server to help your friends stop using certain words or phrases in conversation.

It is written in Rust and based on the [Serenity](https://github.com/serenity-rs/serenity/) API wrapper and is available to self-host as an OCI container image.

[Add TabooBot to your server!](https://discord.com/api/oauth2/authorize?client_id=871186091263275018&permissions=0&scope=bot%20applications.commands)

![Demo screenshot](docs/assets/screenshot.png)

After adding TabooBot to your server, these commands are available:
```
/add <phrase>               Create a new taboo in this server
/delete                     Remove a taboo in this server
/list [sort, reverse]       List all taboos in this server
                            sort    | [count, phrase, longest streak, last seen]
                            reverse | [True, False]
```


## Requirements
- An OCI compliant container engine like Docker or Podman
- A Redis instance


## Configuration
You will need to [register a new Discord application](https://discordapp.com/developers/applications/). This will give you an `APPLICATION_ID` and allow you to add a bot user, creating a `BOT_TOKEN`.

### Environment Variables
- `APPLICATION_ID`: The Discord application's ID
- `BOT_TOKEN`: The Discord application's bot user token
- `LOG_LEVEL`: Logging verbosity (Default: "warn")
- `LOG_STYLE`: Logging style (Default: "always")
- `REDIS_USER`: A Redis username (Default: "")
- `REDIS_PASSWORD`: The password for `REDIS_USER`
- `REDIS_HOST`: The Redis instance host (Default: "127.0.0.1")
- `REDIS_PORT`: The Redis instance port (Default: "6379")
- `REDIS_DB`: A Redis instance database (Default: "")
- `SENTRY_DSN`: A Sentry compatible DSN for error monitoring (Default: "")
- `USE_ERROR_REPORTING`: Whether or not to send error reports to the `SENTRY_DSN` (Default: false)
- `USE_TRACING`: Whether or not to send traces to Jaeger running locally (Default: false)
    - This is still experimental and will be more configurable in the future


## Usage
The official TabooBot instance is deployed in a Kubernetes cluster using Kustomize. [The official Kustomize repo](https://gitlab.com/tedtramonte/kustomize-taboobot) contains a basic deployment for your own cluster which you can modify to fit your needs.

Using `docker-compose` and a `.env` file, a simple deployment could look something like:
```yaml
services:
    bot:
        image: registry.gitlab.com/tedtramonte/taboobot:latest
        restart: unless-stopped
        environment:
            APPLICATION_ID: ${APPLICATION_ID}
            BOT_TOKEN: ${BOT_TOKEN}
            REDIS_PASSWORD: ${REDIS_PASSWORD}
            REDIS_HOST: redis
            REDIS_PORT: "6379"
    redis:
        image: bitnami/redis:6.2.5
        restart: unless-stopped
        expose:
            - "6379"
        volumes:
            - data:/bitnami/redis/data
        environment:
            REDIS_PASSWORD: ${REDIS_PASSWORD}
volumes:
    data:
```


## Development
The recommended way to make and test changes to the application is to use [Tilt](https://tilt.dev/) to deploy to a local [k3d](https://k3d.io/) Kubernetes cluster, as this will closely match the official environment.
```shell
# Create a k3d cluster with a container registry
# and map localhost:8080 to the cluster loadbalancer
k3d cluster create dev --registry-create dev-registry -p "8080:80@loadbalancer"

tilt up
```

For convenience, in addition to `taboobot` and an instance of `redis` being deployed:
- `jaeger` will be available at [http://localhost:8080/jaeger](http://localhost:8080/jaeger)
- `redis-commander` will be available at [http://localhost:8080/redis-commander](http://localhost:8080/redis-commander)

Adjust the ports as necessary to fit your development environment.


## Contributing
Merge requests are welcome after opening an issue first.
